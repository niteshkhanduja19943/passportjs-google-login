const passport = require('passport')
const GoogleStrategy = require('passport-google-oauth20')
const keys = require('./keys')
const User = require("../models/userModel")
//serealize user
passport.serializeUser((user,done)=>{
    
    done(null,user.id)
})
//deserialze 
passport.deserializeUser((id,done)=>{
    
    User.findById(id).then((user)=>{
        done(null,user.id)

    })
   
})

passport.use(
    new GoogleStrategy({
//option for google starat\
     callbackURL:"/auth/google/redirect",
     clientID:keys.google.clientID,     
     clientSecret:keys.google.clientSecret

},(accessToken,refreshToken,profile,done) =>{
    //passport call back function 
    // check if user already there
    User.findOne({googleID:profile.id}).then((currentUser) =>
    {
        if (currentUser){
            //already have an account
            console.log("User is alredy there ",currentUser)
            done(null,currentUser)
        }
        else{
            // create new user
            new User({
                username:profile.displayName,
                googleID:profile.id
            }).save().then((newUser)=>{
                console.log('new user created'+newUser)
                done(null,newUser)
            })
        }

    })

})
)