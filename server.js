const express = require("express")
const authRoutes = require('./routes/authRoutes')
const app = express()
const passportSetup = require('./config/passport-config')
require("./config/db")
app.set('view engine','ejs')

//setup routes
app.use('/auth',authRoutes)


//route in ejs
app.get('/', (req,res) =>{
    res.render('home')
})


app.listen(3333,() =>{
    console.log(`server  running at 3333`)
})